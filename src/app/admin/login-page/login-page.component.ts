import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../shared/services/auth.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {LoginRequest} from '../../shared/model/login-request-model';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {SharedService} from '../../shared/service/shared.service';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {UserService} from '../../shared/service/user.service';
import {User} from '../../shared/model/user-models';
import {DialogService} from '../../shared/service/dialog.service';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  message: string;
  hide = true;
  roles: string[] = [];

  constructor(public auth: AuthService,
              private router: Router,
              private route: ActivatedRoute,
              private errorHandleService: ErrorHandlerService,
              private sharedService: SharedService,
              private userService: UserService,
              private dialog: DialogService
  ) {

  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      if (params.loginAgain) {
        this.dialog.showDefaultNotification('Введите данные');
      } else if (params.authFailed) {
        this.dialog.showDefaultNotification('Сессия истелка. Введите данные');
      }
    });

    this.form = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true;

    const request: LoginRequest = {
      username: this.form.value.username,
      password: this.form.value.password
    };

    this.auth.login(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 403) {
          this.dialog.showNotification('Неверный пароль', 5000, 'top', 'red-snackbar');
          this.submitted = false;
        } else if (error.status === 404) {
          this.dialog.showNotification(`Логина ${request.username} не существует`, 5000, 'top', 'red-snackbar');
          this.submitted = false;
        }
        return throwError(error);
      }),
      tap(token => {
        this.auth.saveToken(token);
        this.auth.saveUsername(request.username);
        this.sharedService.emitChange(request.username);
        this.form.reset();
        this.router.navigate(['/admin/analytics', 'dashboard']);
        this.submitted = false;
      }),
      switchMap(() => {
        return this.userService.getUserProfile(this.auth.getUsername());
      }),
      tap((user: User) => {
        this.auth.saveRoles(user.roles);
      })
    ).subscribe();
  }
}

