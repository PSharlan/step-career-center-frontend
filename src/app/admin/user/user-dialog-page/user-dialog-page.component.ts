import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SubSink} from 'subsink';
import {User, UserChangePasswordRequest, UserRole, UserUpdateRequest} from '../../../shared/model/user-models';
import {UserService} from '../../../shared/service/user.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {DialogService} from '../../../shared/service/dialog.service';
import {ThemePalette} from '@angular/material/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-user-dialog-page',
  templateUrl: './user-dialog-page.component.html',
  styleUrls: ['./user-dialog-page.component.scss']
})
export class UserDialogPageComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private userService: UserService,
              private errorHandlerService: ErrorHandlerService,
              private dialogService: DialogService) {
  }

  form: FormGroup;
  subs = new SubSink();
  hideOld = true;
  hideNew = true;
  userProfile: User;
  color: ThemePalette = 'warn';
  currentRoles = [];
  availableRoles = [];

  allRoles = [
    UserRole.administrator,
    UserRole.consultant,
    UserRole.eventManager,
    UserRole.storyManager,
    UserRole.vacancyManager,
    UserRole.projectManager
  ];

  ngOnInit() {
    if (this.data.type === 'changePassword') {
      this.form = new FormGroup({
          oldPassword: new FormControl(null, Validators.required),
          newPassword: new FormControl(null, [Validators.minLength(6), Validators.required]),
        },
      );
    } else if (this.data.type === 'userUpdate') {
      this.subs.sink = this.userService.getUserById(this.data.id).subscribe(user => {
        this.userProfile = user;
        this.form = new FormGroup({
          firstName: new FormControl(this.userProfile.firstName, Validators.required),
          lastName: new FormControl(this.userProfile.lastName, Validators.required)
        });
      });
    } else if (this.data.type === 'updateRoles') {
      this.subs.sink = this.userService.getUserById(this.data.id).subscribe(user => {
        this.userProfile = user;
        this.currentRoles = this.userProfile.roles;
        for (const role of this.allRoles) {
          if (!this.currentRoles.includes(role)) {
            this.availableRoles.push(role);
          }
        }
      });
    }
  }

  changePassword() {
    if (this.form.invalid) {
      return;
    }
    const request: UserChangePasswordRequest = {
      email: this.data.email,
      newPassword: this.form.value.newPassword,
      oldPassword: this.form.value.oldPassword
    };
    this.userService.updatePassword(request).subscribe(() => {
      this.form.reset();
      this.dialogService.showDefaultNotification('Пароль успешно изменен');
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  changeUserProfile(): void {
    if (this.form.invalid) {
      return;
    }
    const request: UserUpdateRequest = {
      id: this.data.id,
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName
    };
    this.userService.updateUserProfile(request).subscribe(() => {
      this.form.reset();
      this.dialogService.showDefaultNotification('Данные обновлены');
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  updateRoles(): void {
    this.userService.updateUserRoles(this.data.id, this.currentRoles).subscribe(() => {
      this.dialogService.showDefaultNotification('Роли обновлены');
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
}
