import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SubSink} from 'subsink';
import {UserService} from '../../../shared/service/user.service';
import {UserCreationRequest} from '../../../shared/model/user-models';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {Router} from '@angular/router';

const SUCCESSFUL_CREATION_MESSAGE = 'Пользователь создан';

@Component({
  selector: 'app-user-create-page',
  templateUrl: './user-create-page.component.html',
  styleUrls: ['./user-create-page.component.scss']
})
export class UserCreatePageComponent implements OnInit {
  form: FormGroup;
  subs = new SubSink();

  constructor(
    private userService: UserService,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      userName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.email, Validators.required]),
      password: new FormControl(null, [Validators.minLength(6), Validators.required])
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    const request: UserCreationRequest = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      username: this.form.value.userName,
      email: this.form.value.email,
      password: this.form.value.password
    };
    this.userService.create(request).subscribe(() => {
      this.form.reset();
      this.router.navigate(['/admin/user', 'dashboard']);
      this.dialogService.showDefaultNotification(SUCCESSFUL_CREATION_MESSAGE);
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }
}
