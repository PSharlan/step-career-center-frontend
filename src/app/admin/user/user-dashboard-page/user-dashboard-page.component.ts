import {Component, OnDestroy, OnInit} from '@angular/core';
import {SubSink} from 'subsink';
import {ThemePalette} from '@angular/material/core';
import {User} from '../../../shared/model/user-models';
import {UserService} from '../../../shared/service/user.service';
import {PageEvent} from '@angular/material/paginator';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {UserDialogPageComponent} from '../user-dialog-page/user-dialog-page.component';
import {MatDialog} from '@angular/material/dialog';
import {SUPER_USER} from '../../../shared/constants/role-restrictions-constants';

@Component({
  selector: 'app-user-dashboard-page',
  templateUrl: './user-dashboard-page.component.html',
  styleUrls: ['./user-dashboard-page.component.scss']
})
export class UserDashboardPageComponent implements OnInit, OnDestroy {
  users: User[] = [];
  userProfile: User;
  subs = new SubSink();
  color: ThemePalette = 'warn';
  pageIndex: number;
  pageSize: number;
  length: number;
  pageSizeOptions = [5, 10, 25, 100];
  canEdit = SUPER_USER;

  constructor(
    private userService: UserService,
    private errorHandlerService: ErrorHandlerService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.pageIndex = 0;
    this.pageSize = 5;
    this.length = 100;
    this.getUsersData();
  }

  getUsersData(event?: PageEvent) {
    if (event) {
      this.pageSize = event.pageSize;
      this.pageIndex = event.pageIndex;
    }
    this.subs.sink = this.userService.getAll(this.pageIndex, this.pageSize).subscribe(users => {
      this.users = users;
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  openUpdateProfileDialog(id: string) {
    const dialogRef = this.dialog.open(UserDialogPageComponent, {
      data: {id, type: 'userUpdate'}
    });
    dialogRef.afterClosed().subscribe(() => {
      this.userService.getUserById(id).subscribe( userProfile => {
        const oldUser = this.users.filter(user => user.id === userProfile.id)[0];
        oldUser.firstName = userProfile.firstName;
        oldUser.lastName = userProfile.lastName;
      });
    });
  }

  openUpdateRoleDialog(id: string) {
    const dialogRef = this.dialog.open(UserDialogPageComponent, {
      data: {
        id,
        type: 'updateRoles'
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.userService.getUserById(id).subscribe( userProfile => {
        const oldUser = this.users.filter(user => user.id === userProfile.id)[0];
        oldUser.roles = userProfile.roles;
      });
    });
  }
}
