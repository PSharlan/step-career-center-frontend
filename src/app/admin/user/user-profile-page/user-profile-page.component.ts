import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {UserService} from '../../../shared/service/user.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {User} from '../../../shared/model/user-models';
import {SubSink} from 'subsink';
import {ThemePalette} from '@angular/material/core';
import {UserDialogPageComponent} from '../user-dialog-page/user-dialog-page.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-user-profile-page',
  templateUrl: './user-profile-page.component.html',
  styleUrls: ['./user-profile-page.component.scss']
})
export class UserProfilePageComponent implements OnInit, OnDestroy {
  userProfile: User;
  subs = new SubSink();
  color: ThemePalette = 'warn';

  constructor(private authService: AuthService,
              private userService: UserService,
              private errorHandlerService: ErrorHandlerService,
              public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    const username = this.authService.getUsername();
    this.subs.sink = this.userService.getUserProfile(username).subscribe(userProfile => {
        this.userProfile = userProfile;
      }, error => {
        this.errorHandlerService.handle(error);
      }
    );
  }

  openChangePasswordDialog(): void {
    this.dialog.open(UserDialogPageComponent, {
      data: {
        email: this.userProfile.email,
        type: 'changePassword'
      }
    });
  }

  openUserUpdateDialog(): void {
    this.dialog.open(UserDialogPageComponent, {
      data: {
        id: this.userProfile.id,
        type: 'userUpdate'
      }
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
