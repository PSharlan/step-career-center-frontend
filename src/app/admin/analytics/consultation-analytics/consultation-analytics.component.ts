import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import * as Chart from 'chart.js';
import {ConsultationService} from '../../../shared/service/consultation.service';
import {ConsultationEntryPreview} from '../../../shared/model/consultation-models';
import {FormControl, FormGroup} from '@angular/forms';
import {SubSink} from 'subsink';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

const AMOUNT_DAYS = 7;

@Component({
  selector: 'app-consultation-analytics',
  templateUrl: './consultation-analytics.component.html',
  styleUrls: ['./consultation-analytics.component.scss']
})
export class ConsultationAnalyticsComponent implements OnDestroy, AfterViewInit {

  consultationsDateMap: Map<number, number>;
  consultations: ConsultationEntryPreview[] = [];
  dataConsultation: string[] = [];
  amountConsultation: number[] = [];
  form: FormGroup;
  myConsultationChart: Chart;
  subs = new SubSink();

  constructor(private consultationService: ConsultationService,
              private errorHandleService: ErrorHandlerService) {
  }

  ngAfterViewInit() {
    this.subs.sink = this.consultationService.getAll(0, 100000).subscribe((consultations) => {
      this.consultations = consultations;
      const today = new Date();
      const todayYear = today.getFullYear();
      const todayMonth = today.getMonth();
      const todayDate = today.getDate();
      const startDate = new Date(todayYear, todayMonth, todayDate - AMOUNT_DAYS);
      const tillDate = new Date(todayYear, todayMonth, todayDate + AMOUNT_DAYS);
      this.form = new FormGroup({
        startDate: new FormControl(new Date(startDate)),
        tillDate: new FormControl(new Date(tillDate))
      });
      this.buildGraphic(consultations, startDate, tillDate);
      this.consultationChart();
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  updateChart(): void {
    this.amountConsultation = [];
    this.dataConsultation = [];
    this.subs.sink = this.consultationService.getAll(0, 100000).subscribe((consultations) => {
      const startDate = new Date(this.form.value.startDate);
      const tillDate = new Date(this.form.value.tillDate);
      this.buildGraphic(consultations, startDate, tillDate);
      this.myConsultationChart.data.datasets[0].data = this.amountConsultation;
      this.myConsultationChart.data.labels = this.dataConsultation;
      this.myConsultationChart.update({
        duration: 2000,
        easing: 'easeOutBounce'
      });
    }, error => {
      this.errorHandleService.handle(error);
    });
  }

  private buildGraphic(consultations: ConsultationEntryPreview[], startDate: Date, tillDate: Date): void {
    this.consultationsDateMap = new Map<number, number>();
    while (startDate.getTime() <= tillDate.getTime()) {
      this.consultationsDateMap.set(startDate.getTime(), 0);
      startDate.setDate(startDate.getDate() + 1);
    }
    this.consultations = consultations;
    for (const consultation of this.consultations) {
      const consultationDate = new Date(consultation.assignedAt.split(' ')[0]);
      consultationDate.setHours(0, 0, 0, 0);
      if (this.consultationsDateMap.has(consultationDate.getTime())) {
        this.consultationsDateMap
          .set(consultationDate.getTime(), this.consultationsDateMap.get(consultationDate.getTime()) + 1);
      }
    }
    for (const key of this.consultationsDateMap.keys()) {
      const dateConsultation = new Date(key);
      const year = dateConsultation.getFullYear();
      const month = dateConsultation.getMonth() + 1;
      const dateCons = dateConsultation.getDate();
      this.dataConsultation.push(`${dateCons}.${month}.${year}`);
    }
    this.amountConsultation = Array.from(this.consultationsDateMap.values());
  }

  consultationChart() {
    this.myConsultationChart = new Chart('consultationChart', {
      type: 'line',
      data: {
        // tslint:disable-next-line:max-line-length
        labels: this.dataConsultation,
        datasets: [{
          label: 'График консультаций',
          data: this.amountConsultation,
          borderColor: 'rgb(75, 192, 192, 0.9)',
          borderWidth: 2,
          fill: true,
          lineTension: 0.2
        }],
      },
      options: {
        legend: {
          labels: {
            fontSize: 15
          }
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          yAxes: [{
            display: true,
            ticks: {
              beginAtZero: true,
              stepSize: 1
            }
          }]
        }
      }
    });
    return this.myConsultationChart;
  }
}
