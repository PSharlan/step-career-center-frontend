import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import {StoryService} from '../../../shared/service/story.service';
import {SubSink} from 'subsink';
import * as Chart from 'chart.js';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

@Component({
  selector: 'app-story-analytics',
  templateUrl: './story-analytics.component.html',
  styleUrls: ['./story-analytics.component.scss']
})
export class StoryAnalyticsComponent implements AfterViewInit, OnDestroy {
  subs = new SubSink();
  storyDataChartMap: Map<string, number>;
  keys: string[] = [];
  data: number[] = [];

  constructor(private storyService: StoryService,
              private errorHandleService: ErrorHandlerService) {
  }

  ngAfterViewInit() {
    this.storyDataChartMap = new Map();
    this.subs.sink = this.storyService.getAll(0, 100000).subscribe(stories => {

      for (const story of stories) {
        if (!this.storyDataChartMap.has(story.status)) {
          this.storyDataChartMap.set(story.status, 1);
        } else {
          this.storyDataChartMap.set(story.status, this.storyDataChartMap.get(story.status) + 1);
        }
      }
      this.keys = Array.from(this.storyDataChartMap.keys());
      this.data = Array.from(this.storyDataChartMap.values());
      this.vacancyChart();
    }, error => {
      this.errorHandleService.handle(error);
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  vacancyChart() {
    return new Chart('storyChart', {
      type: 'bar',
      data: {
        labels: this.keys,
        datasets: [{
          label: 'Количество историй по статусу',
          data: this.data,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(200, 206, 0, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              stepSize: 1
            }
          }]
        }
      }
    });
  }

}
