import {Component, OnDestroy, OnInit, AfterViewInit} from '@angular/core';
import * as Chart from 'chart.js';
import {VacancyService} from '../../../shared/service/vacancy.service';
import {SubSink} from 'subsink';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

@Component({
  selector: 'app-vacancy-analytics',
  templateUrl: './vacancy-analytics.component.html',
  styleUrls: ['./vacancy-analytics.component.scss']
})
export class VacancyAnalyticsComponent implements OnInit, AfterViewInit, OnDestroy {
  vacancyDataChartMap: Map<string, number>;
  subs = new SubSink();
  keys: string[] = [];
  data: number[] = [];

  constructor(private vacancyService: VacancyService,
              private errorHandleService: ErrorHandlerService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.vacancyDataChartMap = new Map();
    this.subs.sink = this.vacancyService.getAll(0, 1000000).subscribe((vacancies) => {

      for (const vacancy of vacancies) {
        if (!this.vacancyDataChartMap.has(vacancy.experience)) {
          this.vacancyDataChartMap.set(vacancy.experience, 1);
        } else {
          this.vacancyDataChartMap.set(vacancy.experience, this.vacancyDataChartMap.get(vacancy.experience) + 1);
        }
      }
      const sortedMap = new Map([...this.vacancyDataChartMap].sort());
      this.keys = Array.from(sortedMap.keys());
      this.data = Array.from(sortedMap.values());
      this.vacancyChart();
    }, error => {
      this.errorHandleService.handle(error);
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  vacancyChart() {
    return new Chart('vacancyChart', {
      type: 'bar',
      data: {
        labels: this.keys,
        datasets: [{
          label: 'Количество вакансий по опыту работы',
          data: this.data,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(200, 206, 0, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
}
