import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import {EventService} from '../../../shared/service/event.service';
import {SubSink} from 'subsink';
import * as Chart from 'chart.js';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

@Component({
  selector: 'app-event-analytics',
  templateUrl: './event-analytics.component.html',
  styleUrls: ['./event-analytics.component.scss']
})
export class EventAnalyticsComponent implements AfterViewInit, OnDestroy {
  subs = new SubSink();
  evenDataChartMap: Map<string, number>;
  keys: string[] = [];
  data: number[] = [];

  constructor(private eventService: EventService,
              private errorHandleService: ErrorHandlerService) {
  }

  ngAfterViewInit() {
    this.evenDataChartMap = new Map();
    this.subs.sink = this.eventService.getAll(0, 100000).subscribe(events => {

        for (const event of events) {
          if (!this.evenDataChartMap.has(event.status)) {
            this.evenDataChartMap.set(event.status, 1);
          } else {
            this.evenDataChartMap.set(event.status, this.evenDataChartMap.get(event.status) + 1);
          }
        }
        this.keys = Array.from(this.evenDataChartMap.keys());
        this.data = Array.from(this.evenDataChartMap.values());
        this.vacancyChart();
      }, error => {
        this.errorHandleService.handle(error);
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  vacancyChart() {
    return new Chart('eventChart', {
      type: 'bar',
      data: {
        labels: this.keys,
        datasets: [{
          label: 'Количество мероприятий по статусу',
          data: this.data,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(200, 206, 0, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              stepSize: 1
            }
          }]
        }
      }
    });
  }

}
