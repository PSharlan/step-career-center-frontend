import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {StoryService} from '../../../shared/service/story.service';
import {StoryCreationRequest} from '../../../shared/model/story-models';
import {SubSink} from 'subsink';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {Router} from '@angular/router';

const SUCCESSFUL_CREATION_MESSAGE = 'История была создана';

@Component({
  selector: 'app-stories-create-page',
  templateUrl: './story-create-page.component.html',
  styleUrls: ['./story-create-page.component.scss']
})

export class StoryCreatePageComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  URL_PREFIX = '^(https\\:\\/\\/)(www\\.)(youtube\\.com\\/watch\\?v=)([a-zA-Z0-9\\-_])+';
  form: FormGroup;
  imageName = 'Не выбрано';
  subs = new SubSink();

  constructor(private storyService: StoryService,
              private dialogService: DialogService,
              private errorHandlerService: ErrorHandlerService,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      authorName: new FormControl(null, Validators.required),
      header: new FormControl(null, Validators.required),
      // tslint:disable-next-line:max-line-length
      videoUrl: new FormControl(null, [Validators.pattern(this.URL_PREFIX), Validators.required]),
      description: new FormControl(null, Validators.required),
      image: new FormControl(null, Validators.required)
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const formData = new FormData();
    formData.append('file', this.form.get('image').value);
    this.subs.add(
      this.storyService.uploadImage(formData).subscribe((image) => {
        const id = image.id;

        const request: StoryCreationRequest = {
          authorName: this.form.value.authorName,
          header: this.form.value.header,
          videoUrl: this.form.value.videoUrl,
          description: this.form.value.description,
          imageId: id
        };
        this.storyService.create(request).subscribe(() => {
          this.form.reset();
          this.router.navigate(['/admin/story', 'dashboard']);
          this.dialogService.showDefaultNotification(SUCCESSFUL_CREATION_MESSAGE);
        }, error => {
          this.errorHandlerService.handle(error);
        });
      })
    );
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.imageName = file.name;
      this.form.get('image').setValue(file);
    }
  }
}
