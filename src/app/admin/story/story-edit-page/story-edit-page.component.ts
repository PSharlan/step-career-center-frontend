import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {StoryService} from '../../../shared/service/story.service';
import {Story, StoryUpdateRequest} from '../../../shared/model/story-models';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {SubSink} from 'subsink';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

const SUCCESSFUL_EDIT_MESSAGE = 'История была обновлена';

@Component({
  selector: 'app-stories-edit-page',
  templateUrl: './story-edit-page.component.html',
  styleUrls: ['./story-edit-page.component.scss']
})
export class StoryEditPageComponent implements OnInit, OnDestroy {
  URL_PREFIX = '^(https\\:\\/\\/)(www\\.)(youtube\\.com\\/watch\\?v=)([a-zA-Z0-9\\-_])+';
  form: FormGroup;
  oldStory: Story;
  submitted: boolean;
  subs = new SubSink();

  constructor(
    private storyService: StoryService,
    private route: ActivatedRoute,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.storyService.getById(params.id);
      })
    ).subscribe((story: Story) => {
      this.oldStory = story;
      this.form = new FormGroup({
        authorName: new FormControl(story.authorName, Validators.required),
        header: new FormControl(story.header, Validators.required),
        // tslint:disable-next-line:max-line-length
        videoUrl: new FormControl(story.videoUrl, Validators.pattern(this.URL_PREFIX)),
        description: new FormControl(story.description, Validators.required)
      });
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    const header = 'Вы действительно хотите обновить историю?';
    const actionType = 'update';
    this.dialogService.showDialog(header, actionType).afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.updateStory();
    });
  }

  private updateStory(): void {
    this.submitted = true;
    const request: StoryUpdateRequest = {
      id: this.oldStory.id,
      authorName: this.form.value.authorName,
      header: this.form.value.header,
      videoUrl: this.form.value.videoUrl,
      description: this.form.value.description,
    };
    const sub = this.storyService.update(request).subscribe(() => {
      this.submitted = false;
      this.router.navigate(['/admin/story', 'dashboard']);
      this.dialogService.showDefaultNotification(SUCCESSFUL_EDIT_MESSAGE);
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }
}
