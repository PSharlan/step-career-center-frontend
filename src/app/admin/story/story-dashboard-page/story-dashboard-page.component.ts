import {Component, OnDestroy, OnInit} from '@angular/core';
import {Story} from '../../../shared/model/story-models';
import {StoryService} from '../../../shared/service/story.service';
import {DomSanitizer} from '@angular/platform-browser';
import {ThemePalette} from '@angular/material/core';
import {MatDialog} from '@angular/material/dialog';
import {SubSink} from 'subsink';
import {DialogService} from '../../../shared/service/dialog.service';
import {PageEvent} from '@angular/material/paginator';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {STORY_MANAGER} from '../../../shared/constants/role-restrictions-constants';

const SUCCESSFUL_DELETE_MESSAGE = 'История была удалена';

@Component({
  selector: 'app-stories-dashboard-page',
  templateUrl: './story-dashboard-page.component.html',
  styleUrls: ['./story-dashboard-page.component.scss']
})
export class StoryDashboardPageComponent implements OnInit, OnDestroy {

  constructor(
    private storyService: StoryService,
    private sanitizer: DomSanitizer,
    public dialog: MatDialog,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService
  ) {
  }

  stories: Story[] = [];
  subs = new SubSink();
  color: ThemePalette = 'warn';
  customCollapsedHeight = '70px';
  customExpandedHeight = '70px';
  expansionPanel: boolean;
  pageIndex: number;
  pageSize: number;
  length: number;
  pageSizeOptions = [5, 10, 25, 100];
  canEdit = STORY_MANAGER;

  private static _isExpansionIndicator(target: EventTarget): boolean {
    const expansionIndicatorClass = 'mat-expansion-indicator';

    // @ts-ignore
    return (target.classList && target.classList.contains(expansionIndicatorClass));
  }

  ngOnInit() {
    this.pageIndex = 0;
    this.pageSize = 5;
    this.length = 100;
    this.getStoriesData();
  }

  getStoriesData(event?: PageEvent) {
    if (event) {
      this.pageSize = event.pageSize;
      this.pageIndex = event.pageIndex;
    }
    this.subs.sink = this.storyService.getAll(this.pageIndex, this.pageSize).subscribe(stories => {
      this.stories = stories;
    });
  }

  expandPanel(matExpansionPanel, event): void {
    event.stopPropagation(); // Preventing event bubbling

    if (!StoryDashboardPageComponent._isExpansionIndicator(event.target)) {
      matExpansionPanel.close(); // Here's the magic
    }
  }

  openDialog(id: string) {
    const header = 'Вы действительно хотите удалить историю?';
    const actionType = 'delete';
    this.dialogService.showDialog(header, actionType).afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.subs.sink = this.storyService.delete(id).subscribe(() => {
        this.stories = this.stories.filter(story => story.id !== id);
        this.dialogService.showDefaultNotification(SUCCESSFUL_DELETE_MESSAGE);
      }, error => {
        this.errorHandlerService.handle(error);
      });
    });
  }

  changeStatus(id: string, status: string) {
    let newStatus;
    if (status === 'PUBLISHED') {
      newStatus = 'NEW';
    } else {
      newStatus = 'PUBLISHED';
    }
    const sub = this.storyService.updateStatus(id, newStatus).subscribe(() => {
      this.stories.filter(story => story.id === id)[0].status = newStatus;
    });
    this.subs.add(sub);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  updateVideoUrl(dangerousVideoUrl: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(dangerousVideoUrl);
  }
}
