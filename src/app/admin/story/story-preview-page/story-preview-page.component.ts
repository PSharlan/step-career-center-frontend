import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stories-preview-page',
  templateUrl: './story-preview-page.component.html',
  styleUrls: ['./story-preview-page.component.scss']
})
export class StoryPreviewPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
