import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.isAuthenticated() && !this.authService.isTokenExpired()) {
      return true;
    }
    this.authService.logout();
    this.router.navigate(['/admin', 'login'], {queryParams: {loginAgain: true}});
    return false;
  }

}
