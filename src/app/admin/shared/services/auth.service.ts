import {Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoginRequest} from '../../../shared/model/login-request-model';
import {Observable} from 'rxjs';
import {Token} from '../../../shared/model/token-model';
import {JwtHelperService} from '@auth0/angular-jwt';

const URL_PREFIX = 'http://localhost:8080/api/v1/';
const ACCESS_TOKEN_KEY = 'access';
const USER_KEY = 'user';
const ROLES_KEY = 'roles';

@Injectable({providedIn: 'root'})
export class AuthService {
  roles: string[] = [];

  constructor(private http: HttpClient) {
  }

  @Output() username: string;
  private jwtHelper: JwtHelperService = new JwtHelperService();

  login(request: LoginRequest): Observable<Token> {
    return this.http.post<Token>(URL_PREFIX + 'auth/login', request);
  }

  saveToken(token: Token): void {
    sessionStorage.setItem(ACCESS_TOKEN_KEY, token.token);
  }

  saveUsername(username: string): void {
    sessionStorage.setItem(USER_KEY, username);
  }

  saveRoles(roles: string[]): void {
    this.roles = roles;
    sessionStorage.setItem(ROLES_KEY, this.roles.toString());
  }

  getRoles(): string[] {
    if (this.roles.length === 0) {
     this.roles = this.getRolesFromSessionStorage();
    }
    return this.roles;
  }

  private getRolesFromSessionStorage(): string[] {
    return sessionStorage.getItem(ROLES_KEY).split(',');
  }

  isAuthenticated(): boolean {
    return sessionStorage.getItem(ACCESS_TOKEN_KEY) !== null;
  }

  getToken(): string {
    return sessionStorage.getItem(ACCESS_TOKEN_KEY);
  }

  getUsername(): string {
    this.username = sessionStorage.getItem(USER_KEY);
    return this.username;
  }

  logout(): void {
    sessionStorage.removeItem(ACCESS_TOKEN_KEY);
    sessionStorage.removeItem(USER_KEY);
    sessionStorage.removeItem(ROLES_KEY);
  }

  isTokenExpired(): boolean {
    const tokenDoesNotExist: boolean = this.getToken() === null;
    const tokenIsExpired: boolean = this.jwtHelper.isTokenExpired(this.getToken());
    return tokenDoesNotExist || tokenIsExpired;
  }
}
