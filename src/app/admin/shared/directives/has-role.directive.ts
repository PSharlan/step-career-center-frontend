import {Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {UserService} from '../../../shared/service/user.service';
import {SubSink} from 'subsink';
import {AuthService} from '../services/auth.service';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit, OnDestroy {
  // the role the user must have
  @Input() appHasRole: string[];

  isVisible = false;
  roles: string[] = [];
  subs = new SubSink();

// {ViewContainerRef} viewContainerRef
// the location where we need to render the templateRef
// {TemplateRef<any>} templateRef
// the templateRef to be potentially rendered
  constructor(
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private userService: UserService,
    private auth: AuthService
  ) {
  }

  ngOnInit(): void {
    this.roles = this.auth.getRoles();
    // If he doesn't have any roles, we clear the viewContainerRef
    if (!this.roles) {
      this.viewContainerRef.clear();
    }
    // If the user has the role needed to
    // render this component we can add it
    if (this.roles.some(role => this.appHasRole.includes(role))) {
      // If it is already visible (which can happen if
      // his roles changed) we do not need to add it a second time
      if (!this.isVisible) {
        // We update the `isVisible` property and add the
        // templateRef to the view using the
        // 'createEmbeddedView' method of the viewContainerRef
        this.isVisible = true;
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      }
    } else {
      // If the user does not have the role,
      // we update the `isVisible` property and clear
      // the contents of the viewContainerRef
      this.isVisible = false;
      this.viewContainerRef.clear();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
