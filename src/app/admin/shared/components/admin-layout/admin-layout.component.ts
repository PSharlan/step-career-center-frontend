import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {SharedService} from '../../../../shared/service/shared.service';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
  username: string;

  constructor(
    private router: Router,
    private sharedService: SharedService,
    public auth: AuthService
  ) {
    sharedService.changeEmitted$.subscribe(
      text => {
        this.username = text;
      });
  }

  ngOnInit() {
    this.username = this.auth.getUsername();
  }

  logout(event: Event) {
    event.preventDefault();
    this.auth.logout();
    this.router.navigate(['/admin', 'login']);
  }
}
