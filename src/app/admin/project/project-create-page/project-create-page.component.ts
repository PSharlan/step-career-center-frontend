import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SubSink} from 'subsink';
import {ProjectService} from '../../../shared/service/project-service';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {ProjectCreationRequest} from '../../../shared/model/project-models';
import {forkJoin} from 'rxjs';
import {Router} from '@angular/router';

const SUCCESSFUL_CREATION_MESSAGE = 'Проект был создан';
const URL_PREFIX = '^(https\\:\\/\\/)(www\\.)(youtube\\.com\\/watch\\?v=)([a-zA-Z0-9\\-_])+';

@Component({
  selector: 'app-project-create-component',
  templateUrl: './project-create-page.component.html',
  styleUrls: ['./project-create-page.component.scss']
})
export class ProjectCreatePageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  subs = new SubSink();
  mainImageName = 'Не выбрано';
  imagesName = 'Не выбрано';

  constructor(private projectService: ProjectService,
              private dialogService: DialogService,
              private errorHandlerService: ErrorHandlerService,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      authorName: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      header: new FormControl(null, Validators.required),
      images: new FormControl(null),
      mainImage: new FormControl(null, Validators.required),
      videoUrl: new FormControl(null, Validators.pattern(URL_PREFIX))
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const mainImageFormData = new FormData();
    mainImageFormData.append('file', this.form.get('mainImage').value);
    const uploads = [
      this.projectService.uploadImage(mainImageFormData)
    ];
    const images = this.form.get('images').value;
    for (const image of images) {
      const formData = new FormData();
      formData.append('file', image);
      uploads.push(this.projectService.uploadImage(formData));
    }
    forkJoin(uploads).subscribe((savedImages) => {
      const mainId = savedImages[0].id;
      const ids = [];
      for (let i = 1; i < savedImages.length; i++) {
        ids.push(savedImages[i].id);
      }

      const request: ProjectCreationRequest = {
        authorName: this.form.value.authorName,
        description: this.form.value.description,
        header: this.form.value.header,
        imageIds: ids,
        mainImageId: mainId,
        videoUrl: this.form.value.videoUrl
      };
      this.projectService.create(request).subscribe(() => {
        this.form.reset();
        this.router.navigate(['/admin/project', 'dashboard']);
        this.mainImageName = 'Не выбрано';
        this.imagesName = 'Не выбрано';
        this.dialogService.showDefaultNotification(SUCCESSFUL_CREATION_MESSAGE);
      }, error => {
        this.errorHandlerService.handle(error);
      });
    });
  }

  onImageSelect(event) {
    if (event.target.files.length > 0) {
      const files = event.target.files;
      this.imagesName = '';
      for (const file of files) {
        this.imagesName += file.name + ' ';
      }
      this.form.get('images').setValue(files);
    }
  }

  onMainImageSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.mainImageName = file.name;
      this.form.get('mainImage').setValue(file);
    }
  }
}


