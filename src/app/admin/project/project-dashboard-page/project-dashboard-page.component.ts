import {Component, OnDestroy, OnInit} from '@angular/core';
import {PROJECT_MANAGER} from '../../../shared/constants/role-restrictions-constants';
import {Project, ProjectPreview} from '../../../shared/model/project-models';
import {SubSink} from 'subsink';
import {ProjectService} from '../../../shared/service/project-service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {MatDialog} from '@angular/material/dialog';
import {ThemePalette} from '@angular/material/core';
import {PageEvent} from '@angular/material/paginator';
import {DialogService} from '../../../shared/service/dialog.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-project-dashboard-component',
  templateUrl: './project-dashboard-page.component.html',
  styleUrls: ['./project-dashboard-page.component.scss']
})
export class ProjectDashboardPageComponent implements OnInit, OnDestroy {

  constructor(private projectService: ProjectService,
              private errorHandlerService: ErrorHandlerService,
              public dialog: MatDialog,
              private dialogService: DialogService,
              private sanitizer: DomSanitizer
  ) {
  }

  canEdit = PROJECT_MANAGER;
  projects: ProjectPreview[] = [];
  subs = new SubSink();
  color: ThemePalette = 'warn';
  pageIndex: number;
  pageSize: number;
  length: number;
  pageSizeOptions = [5, 10, 25, 100];
  customCollapsedHeight = '70px';
  customExpandedHeight = '70px';
  expansionPanel: boolean;
  getSingleProject: Map<string, Project>;

  private static _isExpansionIndicator(target: EventTarget): boolean {
    const expansionIndicatorClass = 'mat-expansion-indicator';

    // @ts-ignore
    return (target.classList && target.classList.contains(expansionIndicatorClass));
  }

  ngOnInit() {
    this.getSingleProject = new Map<string, Project>();
    this.pageIndex = 0;
    this.pageSize = 5;
    this.length = 100;
    this.getProjectsData();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  getProjectsData(event?: PageEvent) {
    if (event) {
      this.pageSize = event.pageSize;
      this.pageIndex = event.pageIndex;
    }
    this.subs.sink = this.projectService.getAll(this.pageIndex, this.pageSize).subscribe(projects => {
      this.projects = projects;
      for (const project of projects) {
        this.getSingleProject.set(project.id, null);
      }
      console.log(this.projects);
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  openDialog(id: string) {
    const header = 'Вы действительно хотите удалить проект?';
    const actionType = 'delete';
    this.dialogService.showDialog(header, actionType).afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.subs.sink = this.projectService.delete(id).subscribe(() => {
        this.projects = this.projects.filter(project => project.id !== id);
        this.dialogService.showDefaultNotification('Проект был удален');
      }, error => {
        this.errorHandlerService.handle(error);
      });
    });
  }

  changeStatus(id: string, status: string) {
    let newStatus;
    if (status === 'PUBLISHED') {
      newStatus = 'NEW';
    } else {
      newStatus = 'PUBLISHED';
    }
    this.subs.sink = this.projectService.updateStatus(id, newStatus).subscribe(() => {
      this.projects.filter(project => project.id === id)[0].status = newStatus;
    });
  }

  expandPanel(matExpansionPanel, event): void {
    event.stopPropagation(); // Preventing event bubbling

    if (!ProjectDashboardPageComponent._isExpansionIndicator(event.target)) {
      matExpansionPanel.close(); // Here's the magic
    }
  }

  updateVideoUrl(dangerousVideoUrl: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(dangerousVideoUrl);
  }

  getProject(projectId: string) {
    if (this.getSingleProject.get(projectId) !== null) {
      return;
    }
    this.subs.sink = this.projectService.getById(projectId).subscribe(project => {
      this.getSingleProject.set(projectId, project);
    });
  }
}
