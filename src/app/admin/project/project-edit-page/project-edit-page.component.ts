import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProjectService} from '../../../shared/service/project-service';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Project, ProjectUpdateRequest} from '../../../shared/model/project-models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SubSink} from 'subsink';
import {ThemePalette} from '@angular/material/core';
import {forkJoin} from 'rxjs';

const URL_PREFIX = '^(https\\:\\/\\/)(www\\.)(youtube\\.com\\/watch\\?v=)([a-zA-Z0-9\\-_])+';

@Component({
  selector: 'app-project-edit-page',
  templateUrl: './project-edit-page.component.html',
  styleUrls: ['./project-edit-page.component.scss']
})
export class ProjectEditPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  subs = new SubSink();
  color: ThemePalette = 'warn';
  submitted: boolean;
  project: Project;
  imageIds = [];
  mainId: string;
  imagesName = 'Не выбрано';
  mainImageName = 'Не выбрано';

  constructor(private projectService: ProjectService,
              private dialog: DialogService,
              private errorHandlerService: ErrorHandlerService,
              private route: ActivatedRoute,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.imageIds = [];
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.projectService.getById(params.id);
      })
    ).subscribe((project: Project) => {
      this.project = project;
      this.mainId = this.project.mainImage.id;
      for (const image of this.project.images) {
        if (!this.imageIds.includes(image.id)) { // FIXME REMOVE IF AFTER BUGFIX
          this.imageIds.push(image.id);
        }
      }
      this.form = new FormGroup({
        authorName: new FormControl(project.authorName, Validators.required),
        header: new FormControl(project.header, Validators.required),
        videoUrl: new FormControl(project.videoUrl, Validators.pattern(URL_PREFIX)),
        description: new FormControl(project.description, Validators.required),
        images: new FormControl([]),
        mainImage: new FormControl('')
      });
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.submitted = true;
    const uploads = [];

    if (this.form.get('mainImage').value) {
      const mainImageFormData = new FormData();
      mainImageFormData.append('file', this.form.get('mainImage').value);
      uploads.push(this.projectService.uploadImage(mainImageFormData));
    }

    const images = this.form.get('images').value;
    for (const image of images) {
      const formData = new FormData();
      formData.append('file', image);
      uploads.push(this.projectService.uploadImage(formData));
    }
    if (uploads.length === 0) {
      this.sendRequest();
      return;
    }
    forkJoin(uploads).subscribe(savedImages => {
      if (this.form.get('images').value) {
        this.mainId = savedImages[0].id;
        savedImages = savedImages.filter(image => image.id !== this.mainId);
      }
      for (const image of savedImages) {
        this.imageIds.push(image.id);
      }
      this.sendRequest();
    });
  }

  sendRequest(): void {
    const request: ProjectUpdateRequest = {
      id: this.project.id,
      authorName: this.form.value.authorName,
      header: this.form.value.header,
      videoUrl: this.form.value.videoUrl,
      description: this.form.value.description,
      mainImageId: this.mainId,
      imageIds: this.imageIds
    };
    this.subs.sink = this.projectService.update(request).subscribe(() => {
      this.submitted = false;
      this.router.navigate(['/admin/project', 'dashboard']);
      this.dialog.showDefaultNotification('Проект был обновлен');
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  deleteImage(id: string) {
    this.imageIds = this.imageIds.filter(imageId => imageId !== id);
    this.project.images = this.project.images.filter(image => image.id !== id);
  }

  onImageSelect(event) {
    if (event.target.files.length > 0) {
      const files = event.target.files;
      this.imagesName = '';
      for (const file of files) {
        this.imagesName += file.name + ' ';
      }
      this.form.get('images').setValue(files);
    }
  }

  onMainImageSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.mainImageName = file.name;
      this.form.get('mainImage').setValue(file);
    }
  }
}
