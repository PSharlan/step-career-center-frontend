import {Component, OnDestroy, OnInit} from '@angular/core';
import {ConsultationService} from '../../../shared/service/consultation.service';
import {ConsultationEntryPreview} from '../../../shared/model/consultation-models';
import {SubSink} from 'subsink';
import {MatDialog} from '@angular/material/dialog';
import {ConsultationDialogComponent} from './consultation-dialog/consultation-dialog.component';
import {ThemePalette} from '@angular/material/core';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {CONSULTATION_MANAGER} from '../../../shared/constants/role-restrictions-constants';
import {PageEvent} from '@angular/material/paginator';
import {DialogService} from '../../../shared/service/dialog.service';

const SUCCESSFUL_DELETE_MESSAGE = 'Консультация была удалена';

@Component({
  selector: 'app-consultation-dashboard-page',
  templateUrl: './consultation-dashboard-page.component.html',
  styleUrls: ['./consultation-dashboard-page.component.scss']
})
export class ConsultationDashboardPageComponent implements OnInit, OnDestroy {
  consultations: ConsultationEntryPreview[] = [];
  color: ThemePalette = 'warn';
  subs = new SubSink();
  pageIndex: number;
  pageSize: number;
  length: number;
  pageSizeOptions = [5, 10, 25, 100];
  canEdit = CONSULTATION_MANAGER;

  constructor(private consultationService: ConsultationService,
              public dialog: MatDialog,
              public dialogService: DialogService,
              private errorHandlerService: ErrorHandlerService
  ) {
  }

  ngOnInit() {
    this.pageIndex = 0;
    this.pageSize = 5;
    this.length = 100;
    this.getProjectsData();
  }

  getProjectsData(event?: PageEvent) {
    if (event) {
      this.pageSize = event.pageSize;
      this.pageIndex = event.pageIndex;
    }
    this.subs.sink = this.consultationService.getAll(this.pageIndex, this.pageSize).subscribe(consultations => {
        this.consultations = consultations;
      },
      error => {
        this.errorHandlerService.handle(error);
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  openDialog(id: string) {
    const dialogRef = this.dialog.open(ConsultationDialogComponent, {
      data: {
        value: id
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      const sub = this.consultationService.getById(id).subscribe(foundConsultation => {
        const consultationIndex = this.consultations.findIndex((c) => c.id === foundConsultation.id);
        const firstHalf = this.consultations.slice(0, consultationIndex);
        const secondHalf = this.consultations.slice(consultationIndex + 1, this.consultations.length);
        this.consultations = firstHalf.concat(foundConsultation, secondHalf);
      }, error => {
        this.errorHandlerService.handle(error);
      });
      this.subs.add(sub);
    });
  }

  openDeleteDialog(id: string) {
    const header = 'Вы действительно хотите удалить консультацию?';
    const actionType = 'delete';
    this.dialogService.showDialog(header, actionType).afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.subs.sink = this.consultationService.delete(id).subscribe(() => {
        this.consultations = this.consultations.filter(story => story.id !== id);
        this.dialogService.showDefaultNotification(SUCCESSFUL_DELETE_MESSAGE);
      }, error => {
        this.errorHandlerService.handle(error);
      });
    });
  }
}
