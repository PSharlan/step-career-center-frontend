import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ConsultationService} from '../../../../shared/service/consultation.service';
import {SubSink} from 'subsink';
import {ConsultationEntry, ConsultationEntryUpdateRequest} from '../../../../shared/model/consultation-models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DialogService} from '../../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../../shared/service/error-handler.service';

const SUCCESSFUL_EDIT_MESSAGE = 'Изменения сохранены';

@Component({
  selector: 'app-consultation-dialog',
  templateUrl: './consultation-dialog.component.html',
  styleUrls: ['./consultation-dialog.component.scss']
})
export class ConsultationDialogComponent implements OnInit, OnDestroy {

  subs = new SubSink();
  consultation: ConsultationEntry;
  form: FormGroup;
  editMode = false;
  submitted: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private consultationService: ConsultationService,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService
  ) {
  }

  ngOnInit() {
    const sub = this.consultationService.getById(this.data.value).subscribe(consultationDescription => {
      this.consultation = consultationDescription;
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  changeStatus(newStatus: string) {
    const sub = this.consultationService.changeStatus(this.data.value, newStatus).subscribe(() => {
      this.consultation.consultationStatus = newStatus;
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }

  showForm() {
    this.form = new FormGroup({
      assignedAt: new FormControl(this.consultation.assignedAt.replace(' ', 'T'), Validators.required),
      comment: new FormControl(this.consultation.comment),
      consultationStatus: new FormControl(this.consultation.consultationStatus),
      email: new FormControl(this.consultation.email, Validators.email),
      firstName: new FormControl(this.consultation.firstName, Validators.required),
      group: new FormControl(this.consultation.group),
      lastName: new FormControl(this.consultation.lastName, Validators.required),
      phone: new FormControl(this.consultation.phone, Validators.required),
      student: new FormControl(this.consultation.student),
    });
    this.editMode = true;
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submitted = true;
    const request: ConsultationEntryUpdateRequest = {
      id: this.consultation.id,
      assignedAt: this.form.value.assignedAt.replace('T', ' '),
      comment: this.form.value.comment,
      consultationStatus: this.form.value.consultationStatus,
      email: this.form.value.email,
      firstName: this.form.value.firstName,
      group: this.form.value.group,
      lastName: this.form.value.lastName,
      phone: this.form.value.phone,
      student: this.form.value.student
    };
    const sub = this.consultationService.updateConsultation(request).subscribe(updatedConsultation => {
      this.submitted = false;
      this.dialogService.showDefaultNotification(SUCCESSFUL_EDIT_MESSAGE);
      this.consultation = updatedConsultation;
      this.editMode = false;
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }
}
