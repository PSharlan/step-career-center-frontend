import { Component, OnInit } from '@angular/core';
import {VacancyService} from '../../../shared/service/vacancy.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {VacancyCreationRequest} from '../../../shared/model/vacancy-models';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {Router} from '@angular/router';

const SUCCESSFUL_CREATION_MESSAGE = 'Вакансия была создана';

@Component({
  selector: 'app-vacancy-create-page',
  templateUrl: './vacancy-create-page.component.html',
  styleUrls: ['./vacancy-create-page.component.scss']
})
export class VacancyCreatePageComponent implements OnInit {
  form: FormGroup;

  constructor(
    private vacancyService: VacancyService,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      experience: new FormControl(null, Validators.required),
      company: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required)
    });
  }

  submit() {
      if (this.form.invalid) {
        return;
      }
      const request: VacancyCreationRequest = {
        name: this.form.value.name,
        experience: this.form.value.experience,
        company: this.form.value.company,
        description: this.form.value.description
      };
      this.vacancyService.create(request).subscribe(() => {
        this.form.reset();
        this.router.navigate(['/admin/vacancy', 'dashboard']);
        this.dialogService.showDefaultNotification(SUCCESSFUL_CREATION_MESSAGE);
      }, error => {
        this.errorHandlerService.handle(error);
      });
  }
}
