import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {VacancyService} from '../../../shared/service/vacancy.service';
import {switchMap} from 'rxjs/operators';
import {Vacancy} from '../../../shared/model/vacancy-models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import {SubSink} from 'subsink';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

const SUCCESSFUL_EDIT_MESSAGE = 'Изменения были сохранены';

@Component({
  selector: 'app-vacancy-edit-page',
  templateUrl: './vacancy-edit-page.component.html',
  styleUrls: ['./vacancy-edit-page.component.scss']
})
export class VacancyEditPageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  vacancy: Vacancy;
  submitted = false;
  subs = new SubSink();
  color: ThemePalette = 'warn';

  constructor(
    private route: ActivatedRoute,
    private vacancyService: VacancyService,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.vacancyService.getById(params.id);
      })
    ).subscribe((vacancy: Vacancy) => {
      this.vacancy = vacancy;

      this.form = new FormGroup({
        name: new FormControl(vacancy.name, Validators.required),
        experience: new FormControl(vacancy.experience, Validators.required),
        company: new FormControl(vacancy.company, Validators.required),
        description: new FormControl(vacancy.description, Validators.required)
      });
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submitted = true;
    const vacancyUpdateRequest = {
      id: this.vacancy.id,
      name: this.form.value.name,
      experience: this.form.value.experience,
      company: this.form.value.company,
      description: this.form.value.description
    };
    this.subs.add(
      this.vacancyService.update(vacancyUpdateRequest).subscribe(() => {
        this.submitted = false;
        this.router.navigate(['/admin/vacancy', 'dashboard']);
        this.dialogService.showDefaultNotification(SUCCESSFUL_EDIT_MESSAGE);
      }, error => {
        this.errorHandlerService.handle(error);
      })
    );
  }
}
