import {Component, OnDestroy, OnInit} from '@angular/core';
import {VacancyService} from '../../../shared/service/vacancy.service';
import {VacancyPreview} from '../../../shared/model/vacancy-models';
import {ThemePalette} from '@angular/material/core';
import {MatDialog} from '@angular/material/dialog';
import {DialogService} from '../../../shared/service/dialog.service';
import {SubSink} from 'subsink';
import {PageEvent} from '@angular/material/paginator';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {VACANCY_MANAGER} from '../../../shared/constants/role-restrictions-constants';

const SUCCESSFUL_DELETE_MESSAGE = 'Ваканися была удалена';

@Component({
  selector: 'app-vacancy-dashboard-page',
  templateUrl: './vacancy-dashboard-page.component.html',
  styleUrls: ['./vacancy-dashboard-page.component.scss']
})
export class VacancyDashboardPageComponent implements OnInit, OnDestroy {
  vacancies: VacancyPreview[] = [];
  subs = new SubSink();
  color: ThemePalette = 'warn';
  pageIndex: number;
  pageSize: number;
  length: number;
  pageSizeOptions = [5, 10, 25, 100];
  canEdit = VACANCY_MANAGER;

  constructor(
    private vacancyService: VacancyService,
    public dialog: MatDialog,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService
  ) {
  }

  ngOnInit() {
    this.pageIndex = 0;
    this.pageSize = 5;
    this.length = 100;
    this.getVacanciesData();
  }

  getVacanciesData(event?: PageEvent) {
    if (event) {
      this.pageSize = event.pageSize;
      this.pageIndex = event.pageIndex;
    }
    const sub = this.vacancyService.getAll(this.pageIndex, this.pageSize).subscribe(vacancies => {
      this.vacancies = vacancies;
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }

  delete(id: string) {
    const header = 'Вы действительно хотите удалить вакансию?';
    const actionType = 'delete';
    this.dialogService.showDialog(header, actionType).afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      const sub = this.vacancyService.delete(id).subscribe(() => {
        this.vacancies = this.vacancies.filter(vacancy => vacancy.id !== id);
        this.dialogService.showDefaultNotification(SUCCESSFUL_DELETE_MESSAGE);
      }, error => {
        this.errorHandlerService.handle(error);
      });
      this.subs.add(sub);
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
