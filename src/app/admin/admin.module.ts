import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AdminLayoutComponent} from './shared/components/admin-layout/admin-layout.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {SharedModule} from '../shared/shared.module';
import {AuthGuard} from './shared/services/auth.guard';
import {AlertComponent} from './shared/components/alert/alert.component';
import {AlertService} from './shared/services/alert.service';
import {VacancyCreatePageComponent} from './vacancy/vacancy-create-page/vacancy-create-page.component';
import {VacancyDashboardPageComponent} from './vacancy/vacancy-dashboard-page/vacancy-dashboard-page.component';
import {VacancyEditPageComponent} from './vacancy/vacancy-edit-page/vacancy-edit-page.component';
import {VacancyPreviewPageComponent} from './vacancy/vacancy-preview-page/vacancy-preview-page.component';
import {StoryCreatePageComponent} from './story/story-create-page/story-create-page.component';
import {StoryDashboardPageComponent} from './story/story-dashboard-page/story-dashboard-page.component';
import {StoryEditPageComponent} from './story/story-edit-page/story-edit-page.component';
import {StoryPreviewPageComponent} from './story/story-preview-page/story-preview-page.component';
import {EventCreatePageComponent} from './event/event-create-page/event-create-page.component';
import {EventDashboardPageComponent} from './event/event-dashboard-page/event-dashboard-page.component';
import {EventEditPageComponent} from './event/event-edit-page/event-edit-page.component';
import {EventPreviewPageComponent} from './event/event-preview-page/event-preview-page.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {ConsultationDashboardPageComponent} from './consultation/consultation-dashboard-page/consultation-dashboard-page.component';
import {ConsultationDialogComponent} from './consultation/consultation-dashboard-page/consultation-dialog/consultation-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatPaginatorModule} from '@angular/material/paginator';
import {RedirectPageComponent} from './shared/components/redirect-page/redirect-page.component';
import {UserDashboardPageComponent} from './user/user-dashboard-page/user-dashboard-page.component';
import {UserCreatePageComponent} from './user/user-create-page/user-create-page.component';
import {UserProfilePageComponent} from './user/user-profile-page/user-profile-page.component';
import {UserDialogPageComponent} from './user/user-dialog-page/user-dialog-page.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {HasRoleDirective} from './shared/directives/has-role.directive';
import {ConsultationAnalyticsComponent} from './analytics/consultation-analytics/consultation-analytics.component';
import {ChartsModule} from 'ng2-charts';
import {VacancyAnalyticsComponent} from './analytics/vacancy-analytics/vacancy-analytics.component';
import {AnalyticsDashboardPageComponent} from './analytics/analytics-dashboard-page/analytics-dashboard-page.component';
import {StoryAnalyticsComponent} from './analytics/story-analytics/story-analytics.component';
import {EventAnalyticsComponent} from './analytics/event-analytics/event-analytics.component';
import {ProjectDashboardPageComponent} from './project/project-dashboard-page/project-dashboard-page.component';
import {ProjectCreatePageComponent} from './project/project-create-page/project-create-page.component';
import { ProjectEditPageComponent } from './project/project-edit-page/project-edit-page.component';

@NgModule({
  declarations: [
    AdminLayoutComponent,
    LoginPageComponent,
    AlertComponent,

    VacancyCreatePageComponent,
    VacancyDashboardPageComponent,
    VacancyEditPageComponent,
    VacancyPreviewPageComponent,

    StoryCreatePageComponent,
    StoryDashboardPageComponent,
    StoryEditPageComponent,
    StoryPreviewPageComponent,

    EventCreatePageComponent,
    EventDashboardPageComponent,
    EventEditPageComponent,
    EventPreviewPageComponent,

    ConsultationDashboardPageComponent,

    ConsultationDialogComponent,

    RedirectPageComponent,

    UserDashboardPageComponent,

    UserCreatePageComponent,

    UserProfilePageComponent,

    UserDialogPageComponent,

    HasRoleDirective,

    ConsultationAnalyticsComponent,

    VacancyAnalyticsComponent,

    AnalyticsDashboardPageComponent,

    StoryAnalyticsComponent,

    EventAnalyticsComponent,

    ProjectDashboardPageComponent,

    ProjectCreatePageComponent,

    ProjectEditPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', component: AdminLayoutComponent, children: [
          {path: '', redirectTo: '/admin/login', pathMatch: 'full'},
          {path: 'login', component: LoginPageComponent},

          {path: 'vacancy/dashboard', component: VacancyDashboardPageComponent, canActivate: [AuthGuard]},
          {path: 'vacancy/create', component: VacancyCreatePageComponent, canActivate: [AuthGuard]},
          {path: 'vacancy/:id/edit', component: VacancyEditPageComponent, canActivate: [AuthGuard]},
          {path: 'vacancy/:id/preview', component: VacancyPreviewPageComponent, canActivate: [AuthGuard]},

          {path: 'story/dashboard', component: StoryDashboardPageComponent, canActivate: [AuthGuard]},
          {path: 'story/create', component: StoryCreatePageComponent, canActivate: [AuthGuard]},
          {path: 'story/:id/edit', component: StoryEditPageComponent, canActivate: [AuthGuard]},
          {path: 'story/:id/preview', component: StoryPreviewPageComponent, canActivate: [AuthGuard]},

          {path: 'event/dashboard', component: EventDashboardPageComponent, canActivate: [AuthGuard]},
          {path: 'event/create', component: EventCreatePageComponent, canActivate: [AuthGuard]},
          {path: 'event/:id/edit', component: EventEditPageComponent, canActivate: [AuthGuard]},
          {path: 'event/:id/preview', component: EventPreviewPageComponent, canActivate: [AuthGuard]},

          {path: 'project/create', component: ProjectCreatePageComponent, canActivate: [AuthGuard]},
          {path: 'project/dashboard', component: ProjectDashboardPageComponent, canActivate: [AuthGuard]},
          {path: 'project/:id/edit', component: ProjectEditPageComponent, canActivate: [AuthGuard]},

          {
            path: 'consultation/dashboard',
            component: ConsultationDashboardPageComponent,
            canActivate: [AuthGuard]
          },

          {
            path: 'error/server-error',
            component: RedirectPageComponent,
            canActivate: [AuthGuard]
          },

          {path: 'user/dashboard', component: UserDashboardPageComponent, canActivate: [AuthGuard]},
          {path: 'user/create', component: UserCreatePageComponent, canActivate: [AuthGuard]},
          {path: 'user/profile', component: UserProfilePageComponent, canActivate: [AuthGuard]},

          {
            path: 'analytics/dashboard',
            component: AnalyticsDashboardPageComponent,
            canActivate: [AuthGuard]
          }
        ]
      }
    ]),
    MatExpansionModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatChipsModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ScrollingModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatPaginatorModule,
    DragDropModule,
    ChartsModule
  ],
  entryComponents: [
    ConsultationDialogComponent,
    UserDialogPageComponent
  ],
  exports: [RouterModule],
  providers: [AuthGuard, AlertService, {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'}]
})
export class AdminModule {

}
