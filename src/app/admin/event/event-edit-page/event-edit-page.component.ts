import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EventService} from '../../../shared/service/event.service';
import {Event, EventUpdateRequest} from '../../../shared/model/event-models';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {ThemePalette} from '@angular/material/core';
import {SubSink} from 'subsink';
import {MatDialog} from '@angular/material/dialog';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

const SUCCESSFUL_SAVE_MESSAGE = 'Изменения сохранены';

@Component({
  selector: 'app-event-edit-page',
  templateUrl: './event-edit-page.component.html',
  styleUrls: ['./event-edit-page.component.scss']
})
export class EventEditPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  submitted: boolean;
  oldEvent: Event;
  subs = new SubSink();
  color: ThemePalette = 'warn';

  constructor(
    private eventService: EventService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private dialogService: DialogService,
    private errorHandlerService: ErrorHandlerService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.eventService.getById(params.id);
      })
    ).subscribe((event: Event) => {
      this.oldEvent = event;
      this.form = new FormGroup({
        name: new FormControl(event.name, Validators.required),
        type: new FormControl(event.type, Validators.required),
        address: new FormControl(event.address, Validators.required),
        time: new FormControl(event.time.replace(' ', 'T'), Validators.required),
        description: new FormControl(event.description, Validators.required)
      });
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    const header = 'Вы действительно хотите обновить ивент?';
    const actionType = 'update';
    this.dialogService.showDialog(header, actionType).afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.updateEvent();
    });
  }

  private updateEvent(): void {
    this.submitted = true;
    const request: EventUpdateRequest = {
      id: this.oldEvent.id,
      name: this.form.value.name,
      type: this.form.value.type,
      address: this.form.value.address,
      time: this.form.value.time,
      description: this.form.value.description
    };
    const sub = this.eventService.update(request).subscribe(() => {
      this.submitted = false;
      this.router.navigate(['/admin/event', 'dashboard']);
      this.dialogService.showDefaultNotification(SUCCESSFUL_SAVE_MESSAGE);
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }

}
