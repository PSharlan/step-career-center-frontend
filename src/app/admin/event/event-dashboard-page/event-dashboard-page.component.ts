import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventPreview} from '../../../shared/model/event-models';
import {EventService} from '../../../shared/service/event.service';
import {ThemePalette} from '@angular/material/core';
import {EventEntryPreview} from '../../../shared/model/event-entry-models';
import {EventEntryService} from '../../../shared/service/event-entry.service';
import {MatDialog} from '@angular/material/dialog';
import {SubSink} from 'subsink';
import {DialogService} from '../../../shared/service/dialog.service';
import {PageEvent} from '@angular/material/paginator';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {EVENT_MANAGER} from '../../../shared/constants/role-restrictions-constants';

const SUCCESSFUL_DELETE_MESSAGE = 'Участник был удален';

@Component({
  selector: 'app-event-dashboard-page',
  templateUrl: './event-dashboard-page.component.html',
  styleUrls: ['./event-dashboard-page.component.scss']
})
export class EventDashboardPageComponent implements OnInit, OnDestroy {

  constructor(
    private eventService: EventService,
    private eventEntryService: EventEntryService,
    private dialogService: DialogService,
    public dialog: MatDialog,
    private errorHandlerService: ErrorHandlerService
  ) {
  }

  events: EventPreview[] = [];
  subs = new SubSink();
  color: ThemePalette = 'warn';
  customCollapsedHeight = '100px';
  customExpandedHeight = '100px';
  expansionPanel: boolean;
  eventEntriesMap: Map<string, EventEntryPreview[]>;
  eventDescriptionsMap: Map<string, string>;
  pageIndex: number;
  pageSize: number;
  length: number;
  pageSizeOptions = [5, 10, 25, 100];
  canEdit = EVENT_MANAGER;

  private static _isExpansionIndicator(target: EventTarget): boolean {
    const expansionIndicatorClass = 'mat-expansion-indicator';
    // @ts-ignore
    return (target.classList && target.classList.contains(expansionIndicatorClass));
  }

  ngOnInit() {
    this.eventDescriptionsMap = new Map<string, string>();
    this.eventEntriesMap = new Map<string, EventEntryPreview[]>();
    this.pageIndex = 0;
    this.pageSize = 5;
    this.length = 100;
    this.getEventsData();
  }

  getEventsData(event?: PageEvent) {
    if (event) {
      this.pageSize = event.pageSize;
      this.pageIndex = event.pageIndex;
    }
    const sub = this.eventService.getAll(this.pageIndex, this.pageSize).subscribe(events => {
      this.events = events;
      for (const event of events) {
        this.eventEntriesMap.set(event.id, []);
        this.eventDescriptionsMap.set(event.id, '');
      }
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }

  getDescription(id: string) {
    if (this.eventDescriptionsMap.get(id) !== '') {
      return;
    }
    this.eventService.getById(id).subscribe(event => {
      this.eventDescriptionsMap.set(event.id, event.description);
    }, error => {
      this.errorHandlerService.handle(error);
    });
  }

  expandPanel(matExpansionPanel, event): void {
    event.stopPropagation(); // Preventing event bubbling

    if (!EventDashboardPageComponent._isExpansionIndicator(event.target)) {
      matExpansionPanel.close(); // Here's the magic
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  changeStatus(id: string, newStatus: string) {
    this.eventService.updateStatus(id, newStatus).subscribe(() => {
      this.events.filter(event => event.id === id)[0].status = newStatus;
    });
  }

  getEntry(eventId: string) {
    const sub = this.eventEntryService.getAllEntry(eventId, 0, 100).subscribe(eventEntries => {
      this.eventEntriesMap.set(eventId, eventEntries);
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }

  deleteEventEntry(eventEntryId: string, eventId: string) {
    const sub = this.eventEntryService.deleteEntry(eventEntryId).subscribe(() => {
      const updatedEntries: EventEntryPreview[] = [];
      const entries = this.eventEntriesMap.get(eventId);
      for (const entry of entries) {
        if (entry.id !== eventEntryId) {
          updatedEntries.push(entry);
        }
      }
      this.eventEntriesMap.set(eventId, updatedEntries);
      this.dialogService.showDefaultNotification(SUCCESSFUL_DELETE_MESSAGE);
    }, error => {
      this.errorHandlerService.handle(error);
    });
    this.subs.add(sub);
  }

  openDialog(id: string) {
    const header = 'Вы действительно хотите удалить ивент?';
    const actionType = 'delete';
    this.dialogService.showDialog(header, actionType).afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      const sub = this.eventService.delete(id).subscribe(() => {
        this.events = this.events.filter(event => event.id !== id);
        this.dialogService.showDefaultNotification('Мероприятие было удалено');
      }, error => {
        this.errorHandlerService.handle(error);
      });
      this.subs.add(sub);
    });
  }
}
