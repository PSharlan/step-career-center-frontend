import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EventService} from '../../../shared/service/event.service';
import {EventCreationRequest} from '../../../shared/model/event-models';
import {SubSink} from 'subsink';
import {DialogService} from '../../../shared/service/dialog.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {Router} from '@angular/router';

const DEFAULT_IMAGE_NAME = 'Не выбрано';
const SUCCESSFUL_CREATION_MESSAGE = 'Мероприятие создано';

@Component({
  selector: 'app-event-create-page',
  templateUrl: './event-create-page.component.html',
  styleUrls: ['./event-create-page.component.scss']
})
export class EventCreatePageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  imageName = 'Не выбрано';
  subs = new SubSink();

  constructor(private eventService: EventService,
              private dialogService: DialogService,
              private errorHandlerService: ErrorHandlerService,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      type: new FormControl(null, Validators.required),
      address: new FormControl(null, Validators.required),
      time: new FormControl(null, Validators.required),
      image: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const formData = new FormData();
    formData.append('file', this.form.get('image').value);
    this.subs.sink = this.eventService.uploadImage(formData).subscribe((image) => {
      const id = image.id;

      const request: EventCreationRequest = {
        name: this.form.value.name,
        type: this.form.value.type,
        address: this.form.value.address,
        time: this.form.value.time,
        imageId: id,
        description: this.form.value.description
      };
      this.eventService.create(request).subscribe(() => {
        this.form.reset();
        this.router.navigate(['/admin/event', 'dashboard']);
        this.imageName = DEFAULT_IMAGE_NAME;
        this.dialogService.showDefaultNotification(SUCCESSFUL_CREATION_MESSAGE);
      }, error => {
        this.errorHandlerService.handle(error);
      });
    });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.imageName = file.name;
      this.form.get('image').setValue(file);
    }
  }
}
