import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EventEntryPreview} from '../model/event-entry-models';

const URL_PREFIX = 'http://localhost:8080/api/v1/';

@Injectable({providedIn: 'root'})
export class EventEntryService {
  constructor(private http: HttpClient) {}

  getAllEntry(id: string, page: number, size: number): Observable<EventEntryPreview[]> {
   return this.http.get<EventEntryPreview[]>( URL_PREFIX + `events/${id}/entries?page=${page}&size=${size}`);
  }

  deleteEntry(id: string): Observable<void> {
    return this.http.delete<void>( URL_PREFIX + `events/entries/${id}`);
  }
}
