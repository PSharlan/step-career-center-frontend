import {Injectable} from '@angular/core';
import {NotificationConfig, NotificationDialogContent} from '../model/notification-dialog-models';
import {MatSnackBar, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {NotificationDialogComponent} from '../../admin/shared/components/notification-dialog/notification-dialog.component';
import {MatDialogRef} from '@angular/material/dialog/typings/dialog-ref';
import {ConfirmationDialogContent} from '../model/confirmation-dialog-models';
import {ConfirmationDialogComponent} from '../../admin/shared/components/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material/dialog';

const DEFAULT_DURATION_MS = 3000;
const DEFAULT_VERTICAL_POSITION = 'top';
const DEFAULT_BACKGROUND_COLOR = '';

@Injectable({providedIn: 'root'})
export class DialogService {

  constructor(private snackBar: MatSnackBar,
              public dialog: MatDialog) {
  }


  public showNotification(header: string,
                          duration: number,
                          verticalPosition: MatSnackBarVerticalPosition,
                          panelClass: string): void {
    const notificationConfig: NotificationConfig =
      this.getNotificationConfig(header, duration, verticalPosition, panelClass);
    this.snackBar.openFromComponent(NotificationDialogComponent, notificationConfig);
  }

  public showDefaultNotification(header: string) {
    const config = this.getDefaultNotificationConfig(header);
    this.snackBar.openFromComponent(NotificationDialogComponent, config);
  }

  public showDialog(header: string, actionType: string, description?: string): MatDialogRef<any> {
    const content: ConfirmationDialogContent = {header, actionType, description};
    return this.dialog.open(ConfirmationDialogComponent, {
      data: {content}
    });
  }

  private getNotificationConfig(header: string,
                                duration: number,
                                verticalPosition: MatSnackBarVerticalPosition,
                                panelClass: string): NotificationConfig {
    const content: NotificationDialogContent = {header};
    return {
      data: content,
      duration,
      verticalPosition,
      panelClass
    };
  }

  private getDefaultNotificationConfig(header: string): NotificationConfig {
    return this.getNotificationConfig(header, DEFAULT_DURATION_MS, DEFAULT_VERTICAL_POSITION, DEFAULT_BACKGROUND_COLOR);
  }
}
