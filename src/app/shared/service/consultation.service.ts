import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  ConsultationEntry,
  ConsultationEntryPreview,
  ConsultationEntryUpdateRequest
} from '../model/consultation-models';

const URL_PREFIX = 'http://localhost:8080/api/v1/';

@Injectable({providedIn: 'root'})
export class ConsultationService {
  constructor(private http: HttpClient) {}

  getAll(page: number, size: number): Observable<ConsultationEntryPreview[]> {
    return this.http.get<ConsultationEntryPreview[]>( URL_PREFIX + `consultations/entries?page=${page}&size=${size}`);
  }

  getById(id: string): Observable<ConsultationEntry> {
    return this.http.get<ConsultationEntry>( URL_PREFIX + `consultations/entries/${id}`);
  }

  changeStatus(id: string, status: string): Observable<ConsultationEntry> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<ConsultationEntry>(
      URL_PREFIX + `consultations/entries/${id}/status`, JSON.stringify(status), {headers});
  }

  updateConsultation(request: ConsultationEntryUpdateRequest): Observable<ConsultationEntry> {
    return this.http.put<ConsultationEntry>(URL_PREFIX + `consultations/entries`, request);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(URL_PREFIX + `consultations/entries/${id}`);
  }
}
