import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User, UserChangePasswordRequest, UserCreationRequest, UserUpdateRequest} from '../model/user-models';

const URL_PREFIX = 'http://localhost:8080/api/v1/';

@Injectable({providedIn: 'root'})
export class UserService {
  constructor(private http: HttpClient) {
  }

  create(request: UserCreationRequest): Observable<UserCreationRequest> {
    return this.http.post<UserCreationRequest>(URL_PREFIX + 'users', request);
  }

  getAll(page: number, size: number): Observable<User[]> {
    return this.http.get<User[]>(URL_PREFIX + `users?page=${page}&size=${size}`);
  }

  getUserById(id: string): Observable<User> {
    return this.http.get<User>(URL_PREFIX + `users/${id}`);
  }

  getUserProfile(username: string): Observable<User> {
    return this.http.get<User>(URL_PREFIX + `users/username?username=${username}`);
  }

  updatePassword(request: UserChangePasswordRequest): Observable<User> {
    return this.http.put<User>(URL_PREFIX + 'users/current/password', request);
  }

  updateUserProfile(request: UserUpdateRequest): Observable<User> {
    return this.http.put<User>(URL_PREFIX + 'users', request);
  }

  updateUserRoles(id: number, rolesToSave: string[]): Observable<User> {
    return this.http.put<User>( URL_PREFIX + `users/${id}/roles`, rolesToSave);
  }

}
