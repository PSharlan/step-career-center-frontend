import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class SharedService {
  private emitChangeSource = new Subject<any>();
  // подписавшийся на эту переменную копается в мусорке
  changeEmitted$ = this.emitChangeSource.asObservable();

  // тот кто вызывает этот метод кладет в мусорку change
  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }
}
