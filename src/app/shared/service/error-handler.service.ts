import {Injectable} from '@angular/core';
import {DialogService} from './dialog.service';
import {Router} from '@angular/router';

const SNACKBAR_DURATION_MS = 5000;
const SNACKBAR_VERTICAL_POSITION = 'top';
const SNACKBAR_BACKGROUND_COLOR = 'red-snackbar';
const ERROR_PAGE_URL = '/admin/error/server-error';

@Injectable({providedIn: 'root'})
export class ErrorHandlerService {
  constructor(private dialogService: DialogService, public router: Router) {
  }

  handle(error: any): void {
    if (Math.round(error.status / 100) === 4) {
      const message = this.prepareMessage(error);
      this.dialogService.showNotification(message, SNACKBAR_DURATION_MS, SNACKBAR_VERTICAL_POSITION, SNACKBAR_BACKGROUND_COLOR);
    } else if ((Math.round(error.status / 100) === 5)) {
      this.router.navigate([ERROR_PAGE_URL]);
    }
  }

  private prepareMessage(error: any): string {
    let message;
    const texts = error.error.error;
    const statusCode = error.status;
    for (message of texts) {
      message += texts + '\n';
    }
    message += 'Статус: ' + statusCode;
    return message;
  }
}
