import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Event, EventCreationRequest, EventPreview, EventUpdateRequest} from '../model/event-models';
import {Observable} from 'rxjs';
import {Image} from '../model/image-models';

const URL_PREFIX = 'http://localhost:8080/api/v1/';

@Injectable({providedIn: 'root'})
export class EventService {
  constructor(private http: HttpClient) {}

  create(request: EventCreationRequest): Observable<Event> {
    return this.http.post<Event>( URL_PREFIX + 'events', request);
  }

  getAll(page: number, size: number): Observable<EventPreview[]> {
    return this.http.get<EventPreview[]>(URL_PREFIX + `events?page=${page}&size=${size}`);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>( URL_PREFIX + `events/${id}`);
  }

  uploadImage(data: FormData): Observable<Image> {
    return this.http.post<Image> ( URL_PREFIX + `events/images`, data);
  }

  updateStatus(id: string, status: string): Observable<Event> {
    // content type by default: plain/text
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<Event>(URL_PREFIX + `events/${id}/status`, JSON.stringify(status), {headers});
  }

  getById(id: string): Observable<Event> {
    return this.http.get<Event>( URL_PREFIX + `events/${id}`);
  }

  update(request: EventUpdateRequest): Observable<EventUpdateRequest> {
    return this.http.put<EventUpdateRequest>( URL_PREFIX + `events`, request);
  }
}
