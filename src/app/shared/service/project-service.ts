import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Project, ProjectCreationRequest, ProjectPreview, ProjectUpdateRequest} from '../model/project-models';
import {Observable} from 'rxjs';
import {Image} from '../model/image-models';

const URL_PREFIX = 'http://localhost:8080/api/v1/';

@Injectable({providedIn: 'root'})
export class ProjectService {
  constructor(private http: HttpClient) {
  }

  create(request: ProjectCreationRequest): Observable<ProjectCreationRequest> {
    return this.http.post<ProjectCreationRequest>(`${URL_PREFIX}projects`, request);
  }

  getAll(page: number, size: number): Observable<ProjectPreview[]> {
    return this.http.get<ProjectPreview[]>(`${URL_PREFIX}projects?page=${page}&size=${size}`);
  }

  update(request: ProjectUpdateRequest): Observable<ProjectUpdateRequest> {
    return this.http.put<ProjectUpdateRequest>(`${URL_PREFIX}projects`, request);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${URL_PREFIX}projects/${id}`);
  }

  uploadImage(data: FormData): Observable<Image> {
    return this.http.post<Image>(`${URL_PREFIX}projects/images`, data);
  }

  getById(id: string): Observable<Project> {
    return this.http.get<Project>(`${URL_PREFIX}projects/${id}`);
  }

  updateStatus(id: string, status: string): Observable<Project> {
    // content type by default: plain/text
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<Project>(`${URL_PREFIX}projects/${id}/status`, JSON.stringify(status), {headers});
  }
}
