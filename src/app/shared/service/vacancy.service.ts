import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vacancy, VacancyCreationRequest, VacancyPreview, VacancyUpdateRequest} from '../model/vacancy-models';

const URL_PREFIX = 'http://localhost:8080/api/v1/';

@Injectable({providedIn: 'root'})
export class VacancyService {
  constructor(private http: HttpClient) {}

  create(request: VacancyCreationRequest): Observable<Vacancy> {
    return this.http.post<Vacancy>(URL_PREFIX + 'vacancies', request);
  }

  update(request: VacancyUpdateRequest): Observable<Vacancy> {
    return this.http.put<Vacancy>(URL_PREFIX + 'vacancies', request);
  }

  getById(id: string): Observable<Vacancy> {
    return this.http.get<Vacancy>( URL_PREFIX + `vacancies/${id}`);
  }

  getAll(page: number, size: number): Observable<VacancyPreview[]> {
    return this.http.get<VacancyPreview[]>( URL_PREFIX + `vacancies?page=${page}&size=${size}`);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>( URL_PREFIX + `vacancies/${id}`);
  }
}

