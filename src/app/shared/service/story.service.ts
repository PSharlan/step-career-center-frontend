import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Story, StoryCreationRequest, StoryUpdateRequest} from '../model/story-models';
import {Image} from '../model/image-models';

const URL_PREFIX = 'http://localhost:8080/api/v1/';

@Injectable({providedIn: 'root'})
export class StoryService {
  constructor(private http: HttpClient) {}

  create(request: StoryCreationRequest): Observable<Story> {
    return this.http.post<Story>( URL_PREFIX + 'stories', request);
  }

  updateStatus(id: string, status: string): Observable<Story> {
    // content type by default: plain/text
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<Story>( URL_PREFIX + `stories/${id}/status`, JSON.stringify(status), {headers});
}

  getAll(page: number, size: number): Observable<Story[]> {
    return this.http.get<Story[]>( URL_PREFIX + `stories?page=${page}&size=${size}`);
  }

  getById(id: string): Observable<Story> {
    return  this.http.get<Story>( URL_PREFIX + `stories/${id}`);
  }

  getAllPublished(page: number, size: number): Observable<Story[]> {
    return this.http.get<Story[]>( URL_PREFIX + `stories/published?page=${page}&size=${size}`);
  }

  getAllByStatus(page: number, size: number, status: string): Observable<Story[]> {
    return this.http.get<Story[]>( URL_PREFIX + `stories/status?page=${page}&size=${size}&status=${status}`);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>( URL_PREFIX + `stories/${id}`);
  }

  uploadImage(data: FormData): Observable<Image> {
    return this.http.post<Image>( URL_PREFIX + `stories/images`, data);
  }

  update(request: StoryUpdateRequest): Observable<Story> {
    return this.http.put<Story>( URL_PREFIX + `stories`, request);
  }
}
