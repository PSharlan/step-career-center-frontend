export interface EventEntryPreview {
  id?: string;
  company: string;
  email: string;
  eventId: string;
  eventName: string;
  firstName: string;
  lastName: string;
  student: boolean;
}
