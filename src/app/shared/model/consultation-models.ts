export interface ConsultationEntryPreview {
  assignedAt: string;
  consultationStatus: string;
  createdAt: string;
  id: string;
  lastName: string;
}

export interface ConsultationEntry {
  id: string;
  assignedAt: string;
  comment: string;
  confirmedAt: string;
  consultationStatus: string;
  createdAt: string;
  email: string;
  firstName: string;
  group: string;
  lastName: string;
  phone: string;
  student: boolean;
}

export interface ConsultationEntryUpdateRequest {
  id: string;
  assignedAt: string;
  comment: string;
  consultationStatus: string;
  email: string;
  firstName: string;
  group: string;
  lastName: string;
  phone: string;
  student: boolean;
}
