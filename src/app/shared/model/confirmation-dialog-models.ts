export interface ConfirmationDialogContent {
  header: string;
  actionType: string;
  description?: string;
}
