export interface Vacancy {
  id?: string;
  company: string;
  createdAt: string;
  description: string;
  experience: string;
  name: string;
}

export interface VacancyCreationRequest {
  company: string;
  description: string;
  experience: string;
  name: string;
}

export interface VacancyPreview {
  id?: string;
  name: string;
  company: string;
  experience: string;
}

export interface VacancyUpdateRequest {
  id?: string;
  company: string;
  description: string;
  experience: string;
  name: string;
}
