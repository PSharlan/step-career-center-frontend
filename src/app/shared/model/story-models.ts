import {Image} from './image-models';

export interface Story {
  id?: string;
  authorName: string;
  description: string;
  header:	string;
  image: Image;
  status: string;
  videoEmbedUrl: string;
  videoUrl: string;
}

export interface StoryCreationRequest {
  authorName: string;
  description: string;
  header: string;
  imageId: string;
  videoUrl: string;
}

export interface StoryUpdateRequest {
  id: string;
  authorName: string;
  description: string;
  header: string;
  videoUrl: string;
}
