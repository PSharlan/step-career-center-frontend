export interface Image {
  id?: string;
  main: string;
  preview: string;
}
