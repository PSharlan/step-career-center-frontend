export enum UserRole {
  administrator = 'ROLE_ADMIN',
  consultant = 'ROLE_CONSULTANT',
  eventManager = 'ROLE_EVENT_MANAGER',
  vacancyManager = 'ROLE_VACANCY_MANAGER',
  storyManager = 'ROLE_STORY_MANAGER',
  projectManager = 'ROLE_PROJECT_MANAGER',
  superUser = 'ROLE_SUPERUSER'
}

export interface User {
  email: string;
  firstName: string;
  lastName: string;
  roles: string [];
  username: string;
  id?: string;
}

export interface UserCreationRequest {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  username: string;
}

export interface UserUpdateRequest {
  firstName: string;
  id: string;
  lastName: string;
}

export interface UserChangePasswordRequest {
  email: string;
  newPassword: string;
  oldPassword: string;
}
