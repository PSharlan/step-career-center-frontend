import {MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

export interface NotificationDialogContent {
  header: string;
}

export interface NotificationConfig {
  data: NotificationDialogContent;
  duration: number;
  verticalPosition: MatSnackBarVerticalPosition;
  panelClass: string;
}
