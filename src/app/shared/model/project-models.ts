import {Image} from './image-models';

export interface Project {
  id: string;
  authorName: string;
  description: string;
  header: string;
  images: Image[];
  mainImage: Image;
  status: string;
  videoEmberUrl: string;
  videoUrl: string;
}

export interface ProjectCreationRequest {
  authorName: string;
  description: string;
  header: string;
  imageIds: string[];
  mainImageId: string;
  videoUrl: string;
}

export interface ProjectPreview {
  authorName: string;
  header: string;
  id?: string;
  mainImage: Image;
  status: string;
  videoEmbedUrl: string;
}

export interface ProjectUpdateRequest {
  authorName: string;
  description: string;
  header: string;
  id: string;
  imageIds: string[];
  mainImageId: string;
  videoUrl: string;
}
