import {Image} from './image-models';

export interface Event {
  id?: string;
  address: string;
  description: string;
  name: string;
  image: Image;
  status: string;
  time: string;
  type: string;
}

export interface EventCreationRequest {
  address: string;
  description: string;
  imageId: string;
  name: string;
  time: string;
  type: string;
}

export interface EventUpdateRequest {
  id: string;
  address: string;
  description: string;
  name: string;
  time: string;
  type: string;
}

export interface EventPreview {
  id?: string;
  image: Image;
  name: string;
  status: string;
  time: string;
  type: string;
}
