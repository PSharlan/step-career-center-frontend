import {UserRole} from '../model/user-models';

export const VACANCY_MANAGER = [UserRole.superUser, UserRole.vacancyManager];
export const STORY_MANAGER = [UserRole.superUser, UserRole.storyManager];
export const EVENT_MANAGER = [UserRole.superUser, UserRole.eventManager];
export const CONSULTATION_MANAGER = [UserRole.superUser, UserRole.consultant];
export const PROJECT_MANAGER = [UserRole.superUser, UserRole.projectManager];
export const SUPER_USER = [UserRole.superUser];
