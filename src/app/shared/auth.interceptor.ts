import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {AuthService} from '../admin/shared/services/auth.service';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService,
              private router: Router
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('login')) {
      return next.handle(req);
    }
    // иначе нам нужно добавить в уходящий рекв токен
    const token = this.authService.getToken();
    const updatedReq = req.clone({
      setHeaders: {Authorization: `Bearer_${token}`}
    });
    return next.handle(updatedReq)
      .pipe(
        catchError((error: HttpResponse<any>) => {
          if (error.status === 0) {
            console.log(error);
            this.authService.logout();
            this.router.navigate(['/admin', 'login'], {
              queryParams: {
                authFailed: true
              }
            });
          }
          return throwError(error);
        })
      );
  }

}
